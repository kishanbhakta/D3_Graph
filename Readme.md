## Welcome to My Repo

> This application is a simple Scalable Vector Graphic (SVG) graph built using mock data, to experiment on the many functionalities of D3JS. There is not a specific application for this graph other than for learning how to manipulate all of the feature D3JS has to offer in terms of interactive graphics in JavaScripts.


## Usage

This application contains some simple `gulp` tasks. They are as follows:


- `npm install`: This will install all dependencies specified in the bower.json file
- `gulp watch`: This will launch a Node Server and start the standard `watchlist` task
- `gulp bower`: This will move the bower components into their proper location. This will run during the `watch` task, but you may need to run it manually once in a while
- `gulp handlebars`: This will compile your handlebars (`.hbs`) files for you. Again, `watch` will watch for changes, but if you add new files, you'll need to run this or restart the `gulp watch` command.

There are many more tasks, and you really should read through the `gulpfile.js`, but the ones above will take care of you in most cases.
