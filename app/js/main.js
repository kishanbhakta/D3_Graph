var heartData = [{
      time: 0,
      pulse: 50
    }, {
      time: 1,
      pulse: 100
    }, {
      time: 2,
      pulse: 0
    }, {
      time: 3,
      pulse: -100
    }, {
      time: 4,
      pulse: -25
    }, {
      time: 5,
      pulse: 25
    }, {
      time: 6,
      pulse: 0
    }, {
      time: 7,
      pulse: 100
    }, {
      time: 8,
      pulse: -50
    }, {
      time: 9,
      pulse: 25
    }, {
      time: 10,
      pulse: -25
    }];

    var w = 600;
    var h = 400;

    var svg = d3.select('svg').attr("width", w).attr('height', h);

    var x = d3.scale.linear()
      .domain([0, 10])
      .range([0, w]);

    var y = d3.scale.linear()
      .domain([-150, 150])
      .range([0, h]);

    var line = d3.svg.line()
      .x(function(d) {
        return x(d.time);
      })
      .y(function(d) {
        return y(d.pulse);
      });

    svg.append("path")
      .attr("class", "line")
      .style('stroke', 'black')
      .style('stroke-width', '1')
      .style('fill', 'none')
      .datum(heartData)
      .attr("d", line);

    // Draw transparent rectangle and zoom on mouseup
    var brush = d3.svg.brush()
      .x(x)
      .y(y)
      .on("brushend", function() {
        console.log('brush', brush.extent());
        var extent = brush.extent();
        y.domain([extent[0][1], extent[1][1]]);
        x.domain([extent[0][0], extent[1][0]]);
        svg.select('.line').attr("d", line);
        brush.clear();
        svg.select('.brush').call(brush);
      });

    svg.append("g")
      .attr('class', 'brush')
      .call(brush)
      .selectAll("rect")
      .style('fill-opacity', 0.5)
      .style('fill', 'red');

    //Reset to original graph from Zoomed view

    function reset() {

      x.domain([0, 10]); // reset x domain

      y.domain([-150, 150]);  // reset y domain

      d3.select('.line')
        .attr("d", line); // redraw line
    }

    var d3Brush = this.brush;

    function clearBrush(g) {
      d3.selectAll("g.brush").call(this.brush.clear());
    }


    d3.select('#resetbtn').on('click', function(e) {


      d3.event.preventDefault();

      reset();
      clearBrush();
    });